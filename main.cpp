#include <iostream>
#include <chrono>
#include <unordered_map>
#include "pivot.h"
#include "cut_and_join.h"
#include "timer.h"

#include "slithering_tortoise.h"

#include "short_walk.h"
#include "dimerization.h"

// TODO serialization

int main() {
  constexpr size_t D = 2;
  auto rng = std::default_random_engine{};
  constexpr size_t sizeSAW = 512;
  constexpr size_t nRepetitions = 10;
  auto dimer = Timed{dimerization<D, decltype(rng)&>};
  for (unsigned repetition=0; repetition!=nRepetitions; ++repetition) {
    auto saw = dimer(rng, sizeSAW, 8);
  }
  std::cout << nRepetitions << " SAWs of length " << sizeSAW << " took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(dimer.duration()).count()
            << " milliseconds\n";

  constexpr size_t pivot_samples = pow(sizeSAW, 2);
  constexpr size_t pivot_size = sizeSAW;
  std::vector<Point<D>> pivot_vec(pivot_size);
  std::generate(begin(pivot_vec), end(pivot_vec), [i=0]() mutable { return Point<D>{i++, 0}; });
  Time pivot_timer{};
  auto dist = std::uniform_int_distribution<size_t>(1, pivot_size-2);
  size_t counter = 0;
  auto inter = begin(pivot_vec)+dist(rng);
  for (size_t i=0; i!=pivot_samples; ++i) {
    auto result = pivot_timer(cut_and_join<decltype(begin(pivot_vec)), decltype(rng)&>,
                              begin(pivot_vec), inter, end(pivot_vec), rng);
    if (result.second) {
      ++counter;
    }
    inter = result.first;
    assert(self_avoiding(begin(pivot_vec), inter+1));
    assert(self_avoiding(inter, end(pivot_vec)));
    assert(is_walk(begin(pivot_vec), end(pivot_vec)));
  }
  std::cout << pivot_samples << " samples with " << counter << " transitions took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(pivot_timer.duration()).count()
            << " milliseconds" << std::endl;
  auto walk = SlitheringTortoiseWalk<2, FlexibleSAW>{1.};
  auto maxDistance = 0.;
  constexpr auto N = 1'000'000;
  auto timer = TimerImpl{};
  {
    auto raii = timer.raii();
    for (auto i=0; i!=N; ++i) {
      auto distance = walk.step(rng);
      maxDistance = std::max(maxDistance, distance);
      // std::cout << "distance = " << distance << ", len = " << walk.size() << '\n';
    }
  }
  std::cout << N << " steps took a total of "
            << std::chrono::duration_cast<std::chrono::milliseconds>(timer.duration()).count()
            << " milliseconds\n";
  std::cout << "max = " << maxDistance << std::endl;
}