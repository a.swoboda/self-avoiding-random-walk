//
// Created by Andreas on 28/10/2018.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_PIVOT_H
#define SELF_AVOIDING_RANDOM_WALK_PIVOT_H

#include <vector>
#include <set>
#include <unordered_set>
#include <random>

#include <iterator>
#include <cassert>

#include "point.h"
#include "concat_sa.h"

struct DiagonalReflection {
  static constexpr size_t D = 2;
  enum Which : int { fst=-1, snd=1 };
  Point<D> operator()(Point<D> point) const {
    return Point<D>{{which*point[1], which*point[0]}};
  }
  DiagonalReflection(Which which) : which(static_cast<int>(which)) { assert(abs(which) == 1); }
private:
  int which;
};


template <typename RNG>
inline DiagonalReflection randomDiagonalReflection(RNG&& rng) {
  auto dist = std::bernoulli_distribution{};
  return DiagonalReflection(dist(rng) ? DiagonalReflection::fst : DiagonalReflection::snd);
}


template <typename Iterator, typename SymmetryOperation>
inline bool pivot_step(Iterator from, Iterator pivot, Iterator to, SymmetryOperation g) {
  assert(from <= pivot);
  assert(pivot < to);  // pivot shall be IN the range, not beyond it
  auto helper = [g, offset=*pivot](auto p) { return offset + g(p - offset); };
  // we know that both parts are self-avoiding themselves
  if (concat_sa(from, pivot, pivot, to, helper)) {
    // copy reflected
    std::transform(pivot, to, pivot, helper);
    return true;
  }
  return false;
}

#endif //SELF_AVOIDING_RANDOM_WALK_PIVOT_H
