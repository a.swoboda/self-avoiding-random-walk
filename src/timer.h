//
// Created by Andreas on 01/11/2018.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_TIMER_H
#define SELF_AVOIDING_RANDOM_WALK_TIMER_H

#include <chrono>
#include <atomic>
#include <utility>

struct TimerImpl {
  using clock = std::chrono::steady_clock;
  using Duration = clock::duration;
  using Rep = Duration::rep;
  Duration duration() const { return Duration{rep}; }
  auto raii() { return RAII(this); }
  TimerImpl() = default;
  TimerImpl(TimerImpl const& other) : rep{other.rep.load(std::memory_order_relaxed)} {}
  TimerImpl(TimerImpl&& other) : rep{other.rep.load(std::memory_order_relaxed)} {}
private:
  struct RAII {
    RAII(TimerImpl* receiver) : receiver{receiver} {}
    RAII(RAII const&) = delete;
    RAII& operator =(RAII const&) = delete;
    ~RAII() { receiver->rep.fetch_add((clock::now()-beg).count(), std::memory_order_relaxed); }
    RAII(RAII&& other) = default;
    RAII& operator =(RAII&& rhs) = default;
  private:
    TimerImpl* receiver;
    using TimePoint = clock::time_point;
    TimePoint beg{clock::now()};
  };
  std::atomic<Rep> rep{Rep{}};
};

struct Time : TimerImpl {
  template <typename F, typename...Args>
  auto operator()(F&& f, Args&&...args) {
    auto lock = raii();
    return std::forward<F>(f)(std::forward<Args>(args)...);
  }
};

template <typename F>
struct Timed : TimerImpl {
  Timed(F f) : f(std::move(f)) {}
  template <typename...Args>
  auto operator()(Args&&...args) {
    auto lock = this->raii();
    return f(std::forward<Args>(args)...);
  }
private:
  F f;
};

#endif //SELF_AVOIDING_RANDOM_WALK_TIMER_H
