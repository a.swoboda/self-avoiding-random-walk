//
// Created by ahab on 11/12/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_DIMERIZATION_H
#define SELF_AVOIDING_RANDOM_WALK_DIMERIZATION_H

#include "short_walk.h"
#include "slithering_tortoise.h"


template <size_t D, typename RNG>
inline ShortSAW<D> dimerization(RNG && rng, size_t N, size_t n=8) {
  if (N < n) {
    auto saw = SlitheringTortoiseWalk<D, ShortSAW>{1.};
    while (saw.size() != N) { saw.step(rng); }
    return saw;
  }
  auto left_size = N/2;
  auto right_size = N - left_size;
  auto lhs = dimerization<D>(rng, left_size, n);
  auto rhs = dimerization<D>(rng, right_size, n);
  while (rhs.try_concatenate(lhs)) {
    lhs = dimerization<D>(rng, left_size, n);
    rhs = dimerization<D>(rng, right_size, n);
  }
  return std::move(rhs);
}

#endif //SELF_AVOIDING_RANDOM_WALK_DIMERIZATION_H
