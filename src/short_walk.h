//
// Created by ahab on 11/9/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_SHORT_WALK_H
#define SELF_AVOIDING_RANDOM_WALK_SHORT_WALK_H

#include <vector>
#include <initializer_list>
#include <algorithm>
#include <stdexcept>
#include <ostream>

#include "point.h"
#include "util.h"

// TODO add template parameter for comparison operator
template <typename T>
struct ShortLookup {
  bool insert(T const& t) {
    auto low = std::lower_bound(begin(), end(), t);
    if (low==end() or t < *low) {
      data.insert(low, t);
      return true;
    }
    return false;
  }
  bool contains(T const& t) const {
    return std::binary_search(begin(), end(), t);
  }
  ShortLookup merge(ShortLookup const& other, ShortLookup resource={}) const {
    resource.clear();
    resource.reserve(size()+other.size());
    std::set_union(begin(), end(), other.begin(), other.end(), std::back_inserter(resource.data));
    return std::move(resource);
  }
  template <typename Trafo>
  ShortLookup merge(ShortLookup const& other, Trafo trafo, ShortLookup resource={}) const {
    if (other.empty()) {
      resource = *this;
      return std::move(resource);
    }
    resource.clear();
    resource.reserve(size()+other.size());
    set_union_trafo(begin(), end(), other.begin(), other.end(), std::back_inserter(resource.data), trafo);
    return std::move(resource);
  }
  template <typename Trafo>
  bool disjoint(ShortLookup const& other, Trafo trafo) const {
    return disjoint_trafo(begin(), end(), other.begin(), other.end(), trafo);
  }
  auto size() const noexcept { return data.size(); }
  auto empty() const noexcept { return data.empty(); }
  void reserve(size_t capacity) { data.reserve(capacity); }
  void clear() noexcept { data.clear(); }
private:
  using Container = std::vector<T>;
public:
  using ConstIterator = typename Container::const_iterator;
  auto erase(ConstIterator pos) { return data.erase(pos); }
  auto erase(ConstIterator first, ConstIterator last) { return data.erase(first, last); }
  auto begin() const { return data.begin(); }
  auto end() const { return data.end(); }
private:
  auto begin() { return data.begin(); }
  auto end() { return data.end(); }
  Container data;
};


enum class Direction : int { Forward=1, Backward=-1 };


// the first point is always 0
template <size_t D>
struct ShortSAW {
  bool can_concatenate(ShortSAW const& other) const {
    if (other.lookup.contains(-walk.back())) {  // check zero point
      return false;
    }
    auto trafo = [offset=walk.back()](auto p) { return p+offset; };
    return lookup.disjoint(other.lookup, trafo);
  }
  bool try_concatenate(ShortSAW const& other) {
    if (not can_concatenate(other)) { return false; }
    if (empty()) {
      *this = other;
      return true;
    }
                                                                                                                                                                                                        auto trafo = [offset=walk.back()](auto p) { return p+offset; };
    auto resource = Lookup{};
    resource.reserve(lookup.size() + other.lookup.size());
    lookup = lookup.merge(other.lookup, trafo, std::move(resource));
    walk.reserve(walk.size()+other.walk.size());
    std::transform(begin(other.walk), end(other.walk), std::back_inserter(walk), trafo);
    return true;
  }
  bool attempt(size_t dim, Direction dir) {
    auto candidate = empty() ? firstPoint : walk.back();
    candidate[dim] += static_cast<int>(dir);
    // special precaution for first point (0)
    if (candidate == firstPoint or not lookup.insert(candidate)) { return false; }
    walk.emplace_back(candidate);
    return true;
  }
  void pop() {
    auto [first, last] = std::equal_range(begin(lookup), end(lookup), walk.back());
    lookup.erase(first, last);
    walk.pop_back();
  }
  auto const& back() const { return walk.back(); }
  bool empty() const { return walk.empty(); }
  size_t size() const { return walk.size(); }
  void print(std::ostream& out) const {
    out << "{ ";
    for (auto p : walk) {
      out << p << ", ";
    }
    out << '}';
  }
private:
  static constexpr Point<D> firstPoint{};
  using Walk = std::vector<Point<D>>;
  using Lookup = ShortLookup<Point<D>>;
  Walk walk;
  Lookup lookup;
};


template <size_t D>
std::ostream& operator<<(std::ostream& out, ShortSAW<D> const& saw) {
  saw.print(out);
  return out;
}


template <size_t D>
struct FlexibleSAW {
  bool attempt(size_t dim, Direction dir) {
    auto candidate = empty() ? firstPoint : walk.back();
    candidate[dim] += static_cast<int>(dir);
    if (candidate == firstPoint or not lookup.insert(candidate).second) { return false; }
    walk.emplace_back(candidate);
    return true;
  }
  void pop() {
    lookup.erase(walk.back());
    walk.pop_back();
  }
  auto const& back() const { return walk.back(); }
  constexpr bool empty() const { return walk.empty(); }
  constexpr size_t size() const { return walk.size(); }
private:
  static constexpr Point<D> firstPoint{};
  std::vector<Point<D>> walk;
  std::unordered_set<Point<D>> lookup;
};

#endif //SELF_AVOIDING_RANDOM_WALK_SHORT_WALK_H
