//
// Created by ahab on 10/30/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_CUT_AND_JOIN_H
#define SELF_AVOIDING_RANDOM_WALK_CUT_AND_JOIN_H

#include <iterator>

#include "pivot.h"
#include "util.h"

// inter is part of both walks
template <typename Iterator, typename RNG>
std::pair<Iterator, bool> cut_and_join(Iterator from, Iterator inter, Iterator to, RNG&& rng) {
  // call pivot_step on both parts
  {
    auto pivot = random_in(inter, to, rng);
    auto reflection = randomDiagonalReflection(rng);
    pivot_step(inter, pivot, to, reflection);
  }
  {
    auto rinter = std::make_reverse_iterator(inter);
    // this adds *inter to both walks.
    --rinter;
    auto rfrom = std::make_reverse_iterator(from);
    auto pivot = random_in(rinter, rfrom, rng);
    auto reflection = randomDiagonalReflection(rng);
    pivot_step(rinter, pivot, rfrom, reflection);
  }
  // 'join' and make a new 'cut'
  auto cut = random_in(from+1, to-1, rng);  // clip of edges to prevent empty walks
  // cut is element of both parts, as was inter
  // we now have two parts again: one part has shrunk, the other has grown.
  // we only need to check that the latter is still self-avoiding
  // as the original parts are self-avoiding, let's be clever about checking this
  using std::distance;
  auto shift = distance(cut, inter);
  // if shift < 0, then we need to check the part from...cut
  if (shift < 0) {
    // layout is from...inter...cut...to
    // we need cut+1 because it is part of both walks
    return concat_sa(from, inter, inter, cut+1) ? std::pair{cut, true} : std::pair{inter, true};
  }
  // else, we check the part cut...to
  else {
    // layout is from...cut...inter...to
    return concat_sa(cut, inter, inter, to) ? std::pair{cut, true} : std::pair{inter, false};
  }
}

#endif //SELF_AVOIDING_RANDOM_WALK_CUT_AND_JOIN_H
