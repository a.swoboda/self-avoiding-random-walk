//
// Created by Swoboda.Andreas on 10/22/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_UTIL_H
#define SELF_AVOIDING_RANDOM_WALK_UTIL_H

#include <cstdint>

#include <chrono>
#include <atomic>
#include <utility>
#include <algorithm>
#include <vector>
#include <unordered_set>

template <typename I>
constexpr auto pow(I i, uint32_t e) {
  I result{1};
  if (e%2) { result *= i; }
  e >>= 1;
  while (e) {
    i *= i;
    if (e%2) { result *= i; }
    e >>= 1;
  }
  return result;
}


template <typename Iterator, typename RNG>
Iterator random_in(Iterator from, Iterator to, RNG&& rng) {
  using std::distance;
  auto bound = distance(from, to)-1;
  auto distribution = std::uniform_int_distribution<long>(0, bound);
  return from + distribution(rng);
}




template <typename Iterator>
bool self_avoiding(Iterator from, Iterator to) {
  using value_type = typename std::iterator_traits<Iterator>::value_type;
  auto lookup = std::unordered_set<value_type>{};
  return std::all_of(from, to, [lookup](auto p) mutable { return lookup.emplace(p).second; });
}


template <typename Iterator>
bool is_walk(Iterator from, Iterator to) {
  if (from == to) { return true; }
  auto next = from+1;
  for (; next!=to; ++from, ++next) {
    if (abs(*next - *from) != 1) { break; }
  }
  return next == to;
}


// expects sorted ranges
template <typename InputIterator0, typename InputIterator1>
bool disjoint(InputIterator0 from0, InputIterator0 to0,
              InputIterator1 from1, InputIterator1 to1) {
  while (from0!=to0 and from1!=to1) {
    if (*from0 < *from1) {
      ++from0;
    }
    else if (*from1 < *from0) {
      ++from1;
    }
    else { return false; }
  }
  return true;
}


// expects sorted ranges; as above, but with custom comparison functor
template <typename InputIterator0, typename InputIterator1, typename Less>
bool disjoint(InputIterator0 from0, InputIterator0 to0,
              InputIterator1 from1, InputIterator1 to1,
              Less cmp) {
  while (from0!=to0 and from1!=to1) {
    if (cmp(from0, *from1)) {
      ++from0;
    }
    else if (cmp(*from1, *from0)) {
      ++from1;
    }
    else { return false; }
  }
  return true;
}


// expects sorted ranges; as above, but with custom tranformation
template <typename InputIterator0, typename InputIterator1, typename Trafo>
bool disjoint_trafo(InputIterator0 from0, InputIterator0 to0,
              InputIterator1 from1, InputIterator1 to1,
              Trafo trafo) {
  if (from1==to1) { return true; }
  auto transformed = trafo(*from1);
  while (from0!=to0) {
    if (*from0 < transformed) {
      ++from0;
    }
    else if (transformed < *from0) {
      ++from1;
      if (from1==to1) { return true; }
      transformed = trafo(*from1);
    }
    else { return false; }
  }
  return true;
}


// transform the elements of the second range
template <typename InputIterator0, typename InputIterator1, typename OutputIterator, typename Trafo>
OutputIterator set_union_trafo(InputIterator0 first0, InputIterator0 last0,
                         InputIterator1 first1, InputIterator1 last1,
                         OutputIterator result, Trafo trafo) {
  if (first0 == last0) { return std::transform(first1, last1, result, trafo); }
  if (first1 == last1) { return std::copy(first0, last0, result); }
  auto transformed = *first1;
  while (true) {
    if (*first0 < transformed) {
      *result = *first0;
      ++first0;
      if (first0 == last0) { return std::transform(first1, last1, result, trafo); }
    }
    else if (transformed < *first0) {
      *result = std::move(transformed);
      ++first1;
      if (first1 == last1) { return std::copy(first0, last0, result); }
      transformed = trafo(*first1);
    }
    else {
      *result = *first1;
      ++first0;
      if (first0 == last0) { return std::transform(first1, last1, result, trafo); }
      ++first1;
      if (first1 == last1) { return std::copy(first0, last0, result); }
      transformed = trafo(*first1);
    }
    ++result;
  }
}

#endif //SELF_AVOIDING_RANDOM_WALK_UTIL_H
