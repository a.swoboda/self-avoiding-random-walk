//
// Created by Swoboda.Andreas on 10/22/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_WALK_H
#define SELF_AVOIDING_RANDOM_WALK_WALK_H

#include <cstdint>
#include <bitset>
#include <array>
#include <set>
#include <vector>
#include <random>
#include <unordered_set>
#include <unordered_map>
#include <map>

#include "util.h"
#include "point.h"
#include "short_walk.h"

template <size_t D, template <size_t> class Lookup>
struct SlitheringTortoiseWalk : Lookup<D> {
  using Set = Lookup<D>;
  template <typename RNG>
  double step(RNG && rng);
  explicit SlitheringTortoiseWalk(double beta) : distribution{1./(1.+2*D*beta)} {}
private:
  std::bernoulli_distribution distribution{};
  std::uniform_int_distribution<size_t> dim_dist{0, D-1};
  std::uniform_int_distribution<int> dir_dist{0, 1};
};


template <size_t D, template <size_t> class Lookup>
template <typename RNG>
inline double SlitheringTortoiseWalk<D, Lookup>::step(RNG && rng) {
  if (distribution(rng) and not this->empty()) {  // try to go back
    this->pop();
  }
  else {
    // to ensure deterministic execution
    auto dir = static_cast<Direction>(dir_dist(rng)*2 - 1);
    this->attempt(dim_dist(rng), dir);
  }
  return this->empty() ? 0 : abs(this->back());
}

#endif //SELF_AVOIDING_RANDOM_WALK_WALK_H
