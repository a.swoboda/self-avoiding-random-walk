//
// Created by Swoboda.Andreas on 10/22/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_POINT_H
#define SELF_AVOIDING_RANDOM_WALK_POINT_H

#include <array>
#include <initializer_list>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <ostream>
#include <sstream>


template <size_t D>
struct Point : std::array<int, D> {
  Point operator-() const {
    auto result = Point{};
    std::transform(this->begin(), this->end(), result.begin(), [](auto x) { return -x; });
    return result;
  }
};

// for small dimension (~2), copying is cheaper than the reference!
template <size_t D>
inline Point<D> operator +(Point<D> lhs, Point<D> rhs) {
  Point<D> result;
  std::transform(begin(lhs), end(lhs), begin(rhs), begin(result),
                 [](auto lhs, auto rhs) { return lhs+rhs; });
  return result;
}

// for small dimension (~2), copying is cheaper than the reference!
template <size_t D>
inline Point<D> operator -(Point<D> lhs, Point<D> rhs) {
  Point<D> result;
  std::transform(begin(lhs), end(lhs), begin(rhs), begin(result),
                 [](auto lhs, auto rhs) { return lhs-rhs; });
  return result;
}

// Euclidean distance
template <size_t D>
inline double abs(Point<D> point) {
  return sqrt(std::accumulate(begin(point), end(point), 0, [](auto acc, auto i) { return acc + i*i; }));
}

template <size_t D>
inline std::ostream& operator<<(std::ostream& out, Point<D> point) {
  out << "{ ";
  for (auto x : point) {
    out << x << ", ";
  }
  out << "}";
  return out;
}

template <size_t D>
inline std::string to_string(Point<D> p) {
  auto stream = std::stringstream{};
  stream << p;
  return stream.str();
}

namespace std {
  template <size_t D>
  struct hash<Point<D>> {
    size_t operator()(Point<D> point) const noexcept {
      size_t seed = 0x9e3779b9;
      return std::accumulate(begin(point), end(point), seed,
                             [=](auto acc, auto x) { return acc^(x + 0x9e3779b9 + (acc<<6) + (acc>>2)); });
    }
  };
}  // namespace std

#endif //SELF_AVOIDING_RANDOM_WALK_POINT_H
