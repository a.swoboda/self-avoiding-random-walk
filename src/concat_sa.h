//
// Created by Andreas on 01/11/2018.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_CONCAT_SA_H
#define SELF_AVOIDING_RANDOM_WALK_CONCAT_SA_H

#include <iterator>
#include <vector>
#include <algorithm>

// if we have two self-avoiding walks,
// then check that their concatenation
//   [from0...to0) ++ [from1...to1)
// is also self-avoiding (does not check that it actually is a walk, though)
template <typename Iterator>
bool concat_sa(Iterator from0, Iterator to0, Iterator from1, Iterator to1) {
  // construct a lookup table from the smaller range
  using value_type = typename std::iterator_traits<Iterator>::value_type;
  using Table = std::vector<value_type>;
  using std::distance;
  using std::begin;
  using std::end;
  auto fst_shorter = distance(from0, to0) < distance(from1, to1);
  if (fst_shorter) {
    using std::swap;
    swap(from0, from1);
    swap(to0, to1);
  }
  auto lookup = Table(from1, to1);
  std::sort(begin(lookup), end(lookup));
  return std::none_of(from0, to0, [b=begin(lookup), e=end(lookup)](auto p) {
                        return std::binary_search(b, e, p);
                      });
}


// as above, but with a unary predicate that is called on all values in the second range
template <typename Iterator, typename UnaryPredicate>
bool concat_sa(Iterator from0, Iterator to0, Iterator from1, Iterator to1, UnaryPredicate predicate) {
  // construct a lookup table from the smaller range
  using value_type = typename std::iterator_traits<Iterator>::value_type;
  using Table = std::vector<value_type>;
  using std::distance;
  using std::begin;
  using std::end;
  auto fst_shorter = distance(from0, to0) < distance(from1, to1);
  if (fst_shorter) {
    auto lookup = Table(from0, to0);
    std::sort(begin(lookup), end(lookup));
    return std::none_of(from1, to1, [b=begin(lookup), e=end(lookup), predicate](auto p) {
                          return std::binary_search(b, e, predicate(p));
                        });
  }
  else {
    auto lookup = Table{};
    lookup.reserve(distance(from1, to1));
    std::transform(from1, to1, std::back_inserter(lookup), predicate);
    std::sort(begin(lookup), end(lookup));
    return std::none_of(from0, to0, [b=begin(lookup), e=end(lookup)](auto p) {
                          return std::binary_search(b, e, p);
                        });
  }
}

#endif //SELF_AVOIDING_RANDOM_WALK_CONCAT_SA_H
