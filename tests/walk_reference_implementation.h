//
// Created by ahab on 10/24/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_WALK_REFERENCE_IMPLEMENTATION_H
#define SELF_AVOIDING_RANDOM_WALK_WALK_REFERENCE_IMPLEMENTATION_H

#include <vector>

#include "point.h"

#include <random>
#include <algorithm>

struct WalkReferenceImplementation {
  static constexpr size_t D = 2;
  enum Dir : uint8_t { Left = 0, Right = 1, Up = 2, Down = 3};
  using Dirs = std::vector<Dir>;
  WalkReferenceImplementation()
    : points{Point<D>{0, 0}}
    , possibilities{Dirs{Left, Right, Up, Down}} { }
  void step() {
    while (possibilities.back().empty()) {
      pop_back();
    }
    size_t randomIndex = std::uniform_int_distribution<size_t>(0, possibilities.back().size())(generator);
    // adjust possibilities for points.back()
    auto dir = possibilities.back()[randomIndex];
    possibilities.back().erase(possibilities.back().begin()+randomIndex);
    // push back new point
    points.push_back(go(points.back(), dir));
    // adjust possibilities for new points.back()
    Dirs sampleFrom{Left, Right, Up, Down};
    sampleFrom.erase(std::remove_if(begin(sampleFrom), end(sampleFrom),
                                    [this](Dir dir) { return has(go(points.back(), dir)); }),
                     end(sampleFrom));
    possibilities.push_back(std::move(sampleFrom));
    return;
  }
  std::vector<Point<D>> possibleNextPoints() const {
    std::vector<Point<D>> result(possibilities.back().size());
    using std::begin; using std::end;
    std::transform(begin(possibilities.back()), end(possibilities.back()), begin(result),
                   [this](auto dir) { return go(points.back(), dir); });
    return result;
  }
  void pop_back() {
    points.pop_back();
    possibilities.pop_back();
  }
  static Dir invert(Dir dir) {
    switch (dir) {
      case Left:
        return Right;
      case Right:
        return Left;
      case Up:
        return Down;
      case Down:
        return Up;
    }
  }
  static Point<D> go(Point<D> from, Dir dir) {
    switch (dir) {
      case Left:
        return from + Point<D>{-1, 0};
      case Right:
        return from + Point<D>{ 1, 0};
      case Up:
        return from + Point<D>{ 0, 1};
      case Down:
        return from + Point<D>{ 0,-1};
    }
  }
  bool has(Point<D> point) const {
    return std::find(begin(points), end(points), point) != end(points);
  }
private:
  std::vector<Point<D>> points;
  std::vector<Dirs> possibilities;
  std::default_random_engine generator{};
};


#endif //SELF_AVOIDING_RANDOM_WALK_WALK_REFERENCE_IMPLEMENTATION_H
