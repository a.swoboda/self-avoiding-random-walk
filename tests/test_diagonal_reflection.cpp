//
// Created by Andreas on 01/11/2018.
//

#include "macros.h"
#include "pivot.h"

int main() {
  DiagonalReflection fst = {DiagonalReflection::fst};
  DiagonalReflection snd = {DiagonalReflection::snd};
  auto point = Point<2>{2, 1};
  auto point0 = Point<2>{-1,-2};
  auto point1 = Point<2>{ 1, 2};
  ASSERT_EQUAL(point0, fst(point));
  ASSERT_EQUAL(point1, snd(point));
  ASSERT_EQUAL(Point<2>{}, fst(Point<2>{}));
  ASSERT_EQUAL(Point<2>{}, snd(Point<2>{}));
}