//
// Created by ahab on 11/10/18.
//

#include <algorithm>

#include "macros.h"

#include "util.h"

#include "point.h"

int main() {
  ASSERT_THROW(disjoint((int*)nullptr, (int*)nullptr, (int*)nullptr, (int*)nullptr));
  auto sorted0 = std::vector<Point<2>>{{{ 0, 1}},
                                       {{ 1, 0}},
                                       {{ 2, 0}},
                                       {{-1, 0}},
                                       {{-3,-2}}};
  std::sort(begin(sorted0), end(sorted0));
  ASSERT_THROW(disjoint(begin(sorted0), end(sorted0),
                        (Point<2>*)nullptr, (Point<2>*)nullptr));
  auto sorted1 = std::vector<Point<2>>{{{-1,-1}},
                                       {{-1,-2}},
                                       {{-2, 0}},
                                       {{-2, 1}},
                                       {{ 1,-1}},
                                       {{ 1, 2}}};
  std::sort(begin(sorted1), end(sorted1));
  ASSERT_THROW(disjoint(begin(sorted0), end(sorted0), begin(sorted1), end(sorted1)));
  auto sorted2 = sorted0;
  sorted2.push_back(sorted1.back());
  std::sort(begin(sorted2), end(sorted2));
  ASSERT_THROW(not disjoint(begin(sorted1), end(sorted1), begin(sorted2), end(sorted2)));
}
