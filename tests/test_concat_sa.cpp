//
// Created by Andreas on 01/11/2018.
//

#include "macros.h"

#include "concat_sa.h"
#include "point.h"
#include "pivot.h"

int main() {
  std::vector<Point<2>> walk = {Point<2>{-1,-1},
                                Point<2>{ 0,-1},
                                Point<2>{ 0, 0},
                                Point<2>{ 1, 0},
                                Point<2>{ 1, 1}};
  auto pivot = begin(walk)+3;
  ASSERT_THROW(concat_sa(begin(walk), pivot, pivot, end(walk)));
  ASSERT_THROW(not concat_sa(begin(walk), pivot, pivot-1, end(walk)));
  ASSERT_THROW(concat_sa(begin(walk), pivot, pivot, end(walk),
                         DiagonalReflection{DiagonalReflection::snd}));
  ASSERT_THROW(not concat_sa(begin(walk), pivot, pivot, end(walk),
                             DiagonalReflection{DiagonalReflection::fst}));
}