//
// Created by Swoboda.Andreas on 10/22/18.
//

#ifndef SELF_AVOIDING_RANDOM_WALK_MACROS_H
#define SELF_AVOIDING_RANDOM_WALK_MACROS_H

#include <stdexcept>

#define ASSERT_THROW( condition )                             \
{                                                                   \
  if( !( condition ) )                                              \
  {                                                                 \
    throw std::runtime_error(   std::string( __FILE__ )             \
                              + std::string( ":" )                  \
                              + std::to_string( __LINE__ )          \
                              + std::string( " in " )               \
                              + std::string( __PRETTY_FUNCTION__ )  \
    );                                                              \
  }                                                                 \
}

#define ASSERT_EQUAL( x, y )                                  \
{                                                                   \
  if( ( x ) != ( y ) )                                              \
  {                                                                 \
    using std::to_string;                                           \
    throw std::runtime_error(   std::string( __FILE__ )             \
                              + std::string( ":" )                  \
                              + std::to_string( __LINE__ )          \
                              + std::string( " in " )               \
                              + std::string( __PRETTY_FUNCTION__ )  \
                              + std::string( ": " )                 \
                              + to_string( ( x ) )             \
                              + std::string( " != " )               \
                              + to_string( ( y ) )             \
    );                                                              \
  }                                                                 \
}

std::string to_string(...) { return "[...]"; }

#endif //SELF_AVOIDING_RANDOM_WALK_MACROS_H
