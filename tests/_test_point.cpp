//
// Created by ahab on 10/23/18.
//

#include "macros.h"

#include "point.h"

int main() {
  auto point = Point<2>{1, 1};
  point = point + Point<2>{-2, -2};
  auto cmp_point = Point<2>{-1, -1};
  ASSERT_EQUAL(point[0], cmp_point[0]);
  ASSERT_EQUAL(point[1], cmp_point[1]);
  ASSERT_THROW(abs(abs(point) - sqrt(2)) < 1.e-15);
}
