//
// Created by Andreas on 01/11/2018.
//

#include "pivot.h"
#include "point.h"
#include "util.h"
#include "macros.h"

int main() {
  std::vector<Point<2>> vec = {Point<2>{ 0, 0},
                               Point<2>{ 1, 0},
                               Point<2>{ 1,-1},
                               Point<2>{ 0,-1},
                               Point<2>{-1,-1}};
  std::vector<Point<2>> cmp = {Point<2>{ 0, 0},
                               Point<2>{ 1, 0},
                               Point<2>{ 1,-1},
                               Point<2>{ 0,-1},
                               Point<2>{ 0,-2}};
  ASSERT_THROW(not pivot_step(begin(vec), begin(vec)+3, end(vec),
                              DiagonalReflection(DiagonalReflection::fst)));
  ASSERT_THROW(pivot_step(begin(vec), begin(vec)+3, end(vec),
                          DiagonalReflection(DiagonalReflection::snd)));
  ASSERT_EQUAL(vec, cmp);
}